/*
  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/

#ifndef JIVEXML_XAODVERTEXRETRIEVER_H
#define JIVEXML_XAODVERTEXRETRIEVER_H

#include <string>
#include <vector>
#include <map>

#include "JiveXML/IDataRetriever.h"
#include "AthenaBaseComps/AthAlgTool.h"
#include "xAODTracking/VertexContainer.h" 

namespace JiveXML{
  
  /**
   * @class xAODVertexRetriever
   * @brief Retrieves all @c Vertex @c objects (VertexAODCollection etc.)
   *
   *  - @b Properties
   *  - DoWriteAllCollections
   *  - DoWriteHLT
   *  - DoWriteV0
   *  - PrimaryVertexCollection: First collection to be retrieved, displayed
   *      in Atlantis without switching. All other collections are
   *      also retrieved
   *  - SecondaryVertexCollection: First collection to be retrieved, displayed
   *      in Atlantis without switching. All other collections are
   *      also retrieved.
   *  - OtherVertexCollections
   *  - TracksName
   *
   *  - @b Retrieved @b Data
   *    - Usual four-vectors: phi, eta, et etc.
   *    - Associations for clusters and tracks via ElementLink: key/index scheme
   */
  class xAODVertexRetriever : virtual public IDataRetriever,
			      public AthAlgTool {
    
  public:
      
    /// Standard Constructor
    xAODVertexRetriever(const std::string& type,const std::string& name,const IInterface* parent);
      
    virtual StatusCode retrieve(ToolHandle<IFormatTool> &FormatTool);
    /// Puts the variables into a DataMap
    const DataMap getData(const xAOD::VertexContainer*, const std::string &key);
    /// Gets the StoreGate keys for the desired containers
    const std::vector<std::string> getKeys();
    /// Return the name of the data type that is generated by this retriever
    virtual std::string dataTypeName() const { return m_typeName; };

  private:
    
    /// The data type that is generated by this retriever
    const std::string m_typeName = "RVx";

    Gaudi::Property<bool> m_doWriteHLT {this, "DoWriteHLT", false, "Write out other collections that have HLT in the name"};
    Gaudi::Property<bool> m_doWriteAllCollections {this, "DoWriteAllCollections", false, "Write out all Cluster collections"};
    Gaudi::Property<bool> m_doWriteV0 {this, "DoWriteV0", false, "Write out other vertex collections that have V0 in the name"};
    Gaudi::Property<std::string> m_primaryVertexKey {this,"PrimaryVertexCollection","PrimaryVertices", "Name of the Primary Vertex container"};
    Gaudi::Property<std::string> m_secondaryVertexKey {this,"SecondaryVertexCollection","BTagging_AntiKt4EMPFlowSecVtx", "Name of the Secondary Vertex container"};
    Gaudi::Property<std::vector<std::string>> m_otherKeys {this, "OtherVertexCollections", {"BTagging_AntiKt4EMTopoSecVtx"}, "Other collections to be retrieved. If DoWriteAllCollections is set to true all available Vertex collections will be retrieved"};
    Gaudi::Property<std::string>  m_tracksName {this, "TracksName", "InDetTrackParticles_xAOD", "Name of track container"};     
  };
}
#endif
