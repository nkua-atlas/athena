/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "../FillAlignTrkInfo.h"
#include "../FillAlignTRTHits.h"
#include "../TRTCalibrator.h"
#include "../FitTool.h"

DECLARE_COMPONENT( FillAlignTrkInfo )
DECLARE_COMPONENT( FillAlignTRTHits )
DECLARE_COMPONENT( TRTCalibrator )
DECLARE_COMPONENT( FitTool )

